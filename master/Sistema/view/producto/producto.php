<h2 class="page-header" >Modulo Productos</h2>


    <a class="btn btn-primary pull-right" href="?c=producto&a=Crud">Nuevo Producto</a>
<br><br><br>

<table class="table  table-striped  table-hover" id="tabla">
    <thead>
        <tr>
        
            <th style="width:180px; background-color: #5DACCD; color:#fff">Nombre Producto</th>
            <th style=" background-color: #5DACCD; color:#fff">Precio</th>
            <th style=" background-color: #5DACCD; color:#fff">Categoría</th>            
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            <td><?php echo $r->nombre; ?></td>
            <td><?php echo $r->precio; ?></td>
            <td><?php echo $r->categoria; ?></td>
            <td>
                <a  class="btn btn-warning" href="?c=producto&a=Crud&idproducto=<?php echo $r->idproducto; ?>">Editar</a>
            </td>
            <td>
                <a  class="btn btn-danger" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=producto&a=Eliminar&idproducto=<?php echo $r->idproducto; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 

</body>
<script  src="assets/js/datatable.js">  

</script>


</html>
