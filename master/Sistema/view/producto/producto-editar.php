<h2 class="page-header">
    <?php echo $producto->idproducto != null ? $producto->nombre : 'Nuevo Producto'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=producto">Producto</a></li>
  <li class="active"><?php echo $producto->idproducto != null ? $producto->nombre : 'Nuevo Registro'; ?></li>
</ol>

<form id="frm-producto" action="?c=producto&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="idproducto" value="<?php echo $producto->idproducto; ?>" />

    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $producto->nombre; ?>" class="form-control" placeholder="" required autocomplete="off">
    </div>
    
     <div class="form-group">
        <label>Precio</label>
        <input type="text" name="precio" value="<?php echo $producto->precio; ?>" class="form-control" placeholder="" required autocomplete="off">
    </div>

     <div class="form-group">
        <label>Categoría</label>
        <select value="<?php echo $producto->categoria; ?>" class="form-control" name="categoria" id="txtbox">
    <option>Bebidas</option>
    <option>Comida</option>
    <option>Postres</option>
    </select>
    </div>
  
    <hr />

    <div class="text-right">
        <button class="btn btn-primary">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-alumno").submit(function(){
            return $(this).validate();
        });
    })
</script>