<h2 class="page-header">
    <?php echo $categoria->idcategoria != null ? $categoria->nombrecategoria : 'Nueva Categoría'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=categoria">Categoría</a></li>
  <li class="active"><?php echo $categoria->idcategoria != null ? $categoria->nombrecategoria : 'Nuevo Registro'; ?></li>
</ol>

<form id="frm-categoria" action="?c=categoria&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="idcategoria" value="<?php echo $categoria->idcategoria; ?>" />
    
    <div class="form-group">
        <label>Nombre Categoría</label>
        <input type="text" name="nombrecategoria" value="<?php echo $categoria->nombrecategoria; ?>" class="form-control" placeholder="Ingrese el nombre de la categoria" required autocomplete="off">
    </div>
    
    <hr />
    
    <div class="text-right">
        <button class="btn btn-primary">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-alumno").submit(function(){
            return $(this).validate();
        });
    })
</script>