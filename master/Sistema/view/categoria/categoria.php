<h2 class="page-header" >Modulo Categorías</h2>


    <a class="btn btn-primary pull-right" href="?c=categoria&a=Crud">Nueva Categoría</a>
<br><br><br>

<table class="table  table-striped  table-hover" id="tabla">
    <thead>
        <tr>
            <th style="width:180px; background-color: #5DACCD; color:#fff">Id</th>
            <th style="width:180px; background-color: #5DACCD; color:#fff">Nombre</th>
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            <td><?php echo $r->idcategoria; ?></td>
            <td><?php echo $r->nombrecategoria; ?></td>
            <td>
                <a  class="btn btn-warning" href="?c=categoria&a=Crud&idcategoria=<?php echo $r->idcategoria; ?>">Editar</a>
            </td>
            <td>
                <a  class="btn btn-danger" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=categoria&a=Eliminar&idcategoria=<?php echo $r->idcategoria; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 

</body>
<script  src="assets/js/datatable.js">  

</script>


</html>
