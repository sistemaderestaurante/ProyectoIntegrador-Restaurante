<h2 class="page-header" >Modulo Empleados</h2>

    <a href="?c=empleado&a=excel" class="btn btn-info">Excel</a>
    <a href="?c=empleado&a=word" class="btn btn-info">Word</a>
    <a class="btn btn-primary pull-right" href="?c=empleado&a=Crud">Nuevo Empleado</a>
<br><br><br>

<table class="table  table-striped  table-hover" id="tabla">
    <thead>
        <tr>
            <th style="width:180px; background-color: #5DACCD; color:#fff">Nombre</th>
            <th style=" background-color: #5DACCD; color:#fff">Fecha de Nacimiento</th>
            <th style=" background-color: #5DACCD; color:#fff">Dirección</th>
            <th style="width:120px; background-color: #5DACCD; color:#fff">Teléfono</th>
            <th style="width:120px; background-color: #5DACCD; color:#fff">Cargo</th>  
            <th style="width:120px; background-color: #5DACCD; color:#fff">Estado</th>
            <th style="width:120px; background-color: #5DACCD; color:#fff">Usuario</th>              
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            <td><?php echo $r->nombre; ?></td>
            <td><?php echo $r->fecha_nacimiento; ?></td>
            <td><?php echo $r->direccion; ?></td>
            <td><?php echo $r->telefono; ?></td>
            <td><?php echo $r->cargo; ?></td>
            <td><?php echo $r->estado; ?></td>
            <td><?php echo $r->usuario; ?></td>
            
            <td>
                <a  class="btn btn-warning" href="?c=empleado&a=Crud&idempleado=<?php echo $r->idempleado; ?>">Editar</a>
            </td>
            <td>
                <a  class="btn btn-danger" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=empleado&a=Eliminar&idempleado=<?php echo $r->idempleado; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 

</body>
<script  src="assets/js/datatable.js">  

</script>


</html>
