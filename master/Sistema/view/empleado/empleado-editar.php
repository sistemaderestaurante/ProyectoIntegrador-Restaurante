<h2 class="page-header">
    <?php echo $empleado->idempleado != null ? $empleado->nombre : 'Nuevo Empleado'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=empleado">Empleado</a></li>
  <li class="active"><?php echo $empleado->idempleado != null ? $empleado->nombre : 'Nuevo Empleado'; ?></li>
</ol>

<form id="frm-empleado" action="?c=empleado&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="idempleado" value="<?php echo $empleado->idempleado; ?>" />

    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $empleado->nombre; ?>" class="form-control" placeholder="Ingrese su nombre" required autocomplete="off">
    </div>
    
    <div class="form-group">
        <label>Fecha de Nacimiento</label>
        <input type="date" name="fecha_nacimiento" value="<?php echo $empleado->fecha_nacimiento ?>" class="form-control" placeholder="" required autocomplete="off">
    </div>

    <div class="form-group">
        <label>Dirección</label>
        <input type="text" name="direccion" value="<?php echo $empleado->direccion; ?>" class="form-control" placeholder="Ingrese su dirección" required autocomplete="off">
    </div>
    
    <div class="form-group">
        <label>Teléfono</label>
        <input type="text" name="telefono" value="<?php echo $empleado->telefono; ?>" class="form-control" placeholder="Ingrese su teléfono" required autocomplete="off">
    </div>

    <div class="form-group">
        <label>Cargo</label>
        <select value="<?php echo $empleado->cargo; ?>" class="form-control" name="cargo" id="txtbox" required autocomplete="off">
    <option>Seleccione su cargo</option>
    <option>Administrativo</option>
    <option>Gerente</option>
    <option>Chef</option>
    <option>Mesero</option>
    </select>
    </div>

    <div class="form-group">
        <label>Estado</label>
        <select value="<?php echo $empleado->estado; ?>" class="form-control" name="estado" id="txtbox" required>
    <option>Seleccione su estado</option>
    <option>Contratado</option>
    <option>Fijo</option>
    </select>
    </div>

    <div class="form-group">
        <label>Usuario</label>
        <select value="<?php echo $empleado->usuario; ?>" class="form-control" name="usuario" id="txtbox" required>
    <option>Seleccione un usuario</option>
    <option>JacquiV</option>
    <option>JennyR</option>
    <option>Yohana</option>
    <option>Marilu</option>
    <option>Jorge</option>
    <option>Shiryu</option>
    </select>
    </div>

        
    
    <hr />
    
    <div class="text-right">
        <button class="btn btn-primary">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-alumno").submit(function(){
            return $(this).validate();
        });
    })
</script>