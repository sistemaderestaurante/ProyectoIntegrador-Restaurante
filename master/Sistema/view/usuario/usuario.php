<h2 class="page-header">Modulo Usuarios</h2>


    <a class="btn btn-primary pull-right" href="?c=usuario&a=Crud">Nuevo Usuario</a>
<br><br><br>

<table class="table  table-striped  table-hover" id="tabla">
    <thead>
        <tr>
        
            <th style="width:180px; background-color: #5DACCD; color:#fff">Usuario</th>
            <th style=" background-color: #5DACCD; color:#fff">Contraseña</th>         
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            <td><?php echo $r->usuario; ?></td>
            <td>*******</td>
            <td>
                <a  class="btn btn-warning" href="?c=usuario&a=Crud&idusuario=<?php echo $r->idusuario; ?>">Editar</a>
            </td>
            <td>
                <a class="btn btn-danger" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=usuario&a=Eliminar&idusuario=<?php echo $r->idusuario; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 

</body>
<script  src="assets/js/datatable.js">  

</script>


</html>
