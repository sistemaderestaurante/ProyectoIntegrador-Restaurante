<h2 class="page-header">
    <?php echo $usuario->idusuario != null ? $usuario->usuario : 'Nuevo Usuario'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=usuario">Usuarios</a></li>
  <li class="active"><?php echo $usuario->idusuario != null ? $usuario->usuario : 'Nuevo Usuario'; ?></li>
</ol>

<form id="frm-usuario" action="?c=usuario&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="idusuario" value="<?php echo $usuario->idusuario; ?>" />
    
    <div class="form-group">
        <label>Usuario</label>
        <input type="text" name="usuario" value="<?php echo $usuario->usuario; ?>" class="form-control" placeholder="Ingrese su usuario" required autocomplete="off">
    </div>
    
    <div class="form-group">
        <label>Contraseña</label>
        <input type="password" name="pass" value="<?php echo $usuario->pass; ?>" class="form-control" placeholder="Ingrese su contraseña" required autocomplete="off">
    </div>
        
    
    <hr />
    
    <div class="text-right">
        <button class="btn btn-primary">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-alumno").submit(function(){
            return $(this).validate();
        });
    })
</script>