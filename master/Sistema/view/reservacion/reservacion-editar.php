<?php error_reporting(0); ?>
<h2 class="page-header">
    <?php echo $reservacion->idreservacion != null ? $reservacion->nombrecliente : 'Nueva Reservación'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=reservacion">Reservacion</a></li>
  <li class="active"><?php echo $reservacion->idreservacion != null ? $reservacion->nombrecliente : 'Nuevo Registro'; ?></li>
</ol>

<form id="frm-reservacion" action="?c=reservacion&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="idreservacion" value="<?php echo $reservacion->idreservacion; ?>" />
    
    <div class="form-group">
        <label>Nombre del Cliente</label>
        <input type="text" name="nombrecliente" value="<?php echo $reservacion->nombrecliente; ?>" class="form-control" placeholder="Ingrese el nombre del cliente" required autocomplete="off">
    </div>
    
    <div class="form-group">
        <label>N° Personas</label>
        <input type="text" name="personas" value="<?php echo $reservacion->personas; ?>" class="form-control" placeholder="Ingrese el número de personas" required autocomplete="off">
    </div>
    
    <div class="form-group">
        <label>Teléfono</label>
        <input type="text" name="telefono" value="<?php echo $reservacion->telefono; ?>" class="form-control" placeholder="Ingrese teléfono" required autocomplete="off">
    </div>

    <div class="form-group">
        <label>Día</label>
        <input type="date" name="dia" min="2018-01-01" value="<?php echo $reservacion->dia; ?>" class="form-control" required>
    </div>

    <div class="form-group">
        <label>Hora</label>
        <input type="time" name="hora" value="<?php echo $reservacion->hora; ?>" class="form-control" required>
    </div>

    <div class="form-group">
        <label>Observaciones</label>
        <input type="text" name="observaciones" value="<?php echo $reservacion->observaciones; ?>" class="form-control" placeholder="Sin Observaciones" required autocomplete="off">
    </div>
    <div class="form-group">
        <label>Mesa</label>
    <select value="<?php echo $reservacion->mesa; ?>" class="form-control" name="mesa" id="txtbox">
    <option>Seleccione una mesa</option>
    <option>Mesa 1</option>
    <option>Mesa 2</option>
    <option>Mesa 3</option>
    <option>Mesa 4</option>
    <option>Mesa 5</option>
    <option>Mesa 6</option>
    <option>Mesa 7</option>
    <option>Mesa 8</option>
    <option>Mesa 9</option>
    <option>Mesa 10</option>
    <option>Mesa 11</option>
    <option>Mesa 12</option>
    <option>Mesa 13</option>
    <option>Mesa 14</option>
    <option>Mesa 15</option>
    <option>Mesa 16</option>
    <option>Mesa 17</option>
    <option>Mesa 18</option>
    <option>Mesa 19</option>
    </select>
    </td>
    </tr>
    </div>

    

    <hr />

    <div class="text-right">
        <button class="btn btn-primary">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-alumno").submit(function(){
            return $(this).validate();
        });
    })
</script>