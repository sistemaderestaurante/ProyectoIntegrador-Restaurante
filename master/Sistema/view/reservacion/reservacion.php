<h2 class="page-header">Modulo Reservación</h2>

    <a href="?c=reservacion&a=excel" class="btn btn-info">Excel</a>
    <a href="?c=reservacion&a=word" class="btn btn-info">Word</a>
    <a class="btn btn-primary pull-right" href="?c=reservacion&a=Crud">Nueva Reservación</a>
<br><br><br>

<table class="table  table-striped  table-hover" id="tabla">
    <thead>
        <tr>
            <th style="width:180px; background-color: #5DACCD; color:#fff">Nombre del Cliente</th>
            <th style=" background-color: #5DACCD; color:#fff">N° Personas</th>
            <th style=" background-color: #5DACCD; color:#fff">Teléfono</th>
            <th style="width:120px; background-color: #5DACCD; color:#fff">Día</th>
            <th style=" background-color: #5DACCD; color:#fff">Hora</th>
            <th style=" background-color: #5DACCD; color:#fff">Observaciones</th>
            <th style=" background-color: #5DACCD; color:#fff">Mesa</th>          
            <center><th style="width:60px; background-color: #5DACCD; color:#fff"></th>
            <th style="width:60px; background-color: #5DACCD; color:#fff">Acciones</th></center>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            <td><?php echo $r->nombrecliente; ?></td>
            <td><?php echo $r->personas; ?></td>
            <td><?php echo $r->telefono; ?></td>
            <td><?php echo $r->dia; ?></td>
            <td><?php echo $r->hora; ?></td>
            <td><?php echo $r->observaciones; ?></td>
            <td><?php echo $r->mesa; ?></td>
            <td>
                <a  class="btn btn-warning" href="?c=reservacion&a=Crud&idreservacion=<?php echo $r->idreservacion; ?>">Editar</a>
            </td>
            <td>
                <a  class="btn btn-danger" onclick="javascript:return confirm('¿Seguro que quiere eliminar este registro?');" href="?c=reservacion&a=Eliminar&idreservacion=<?php echo $r->idreservacion; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 

</body>
<script  src="assets/js/datatable.js">  

</script>


</html>
