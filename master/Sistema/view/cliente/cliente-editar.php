<h2 class="page-header">
    <?php echo $cliente->idcliente != null ? $cliente->nombre : 'Nuevo Cliente'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=cliente">Cliente</a></li>
  <li class="active"><?php echo $cliente->idcliente != null ? $cliente->nombre : 'Nuevo Registro'; ?></li>
</ol>

<form id="frm-cliente" action="?c=cliente&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="idcliente" value="<?php echo $cliente->idcliente; ?>" />
    
    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $cliente->nombre; ?>" class="form-control" placeholder="Ingrese su nombre" required autocomplete="off">
    </div>
    
    <div class="form-group">
        <label>Apellido</label>
        <input type="text" name="apellido" value="<?php echo $cliente->apellido; ?>" class="form-control" placeholder="Ingrese su apellido" required autocomplete="off">
    </div>
    
     <div class="form-group">
        <label>Telefono</label>
        <input type="text" name="telefono" value="<?php echo $cliente->telefono; ?>" class="form-control" placeholder="Ingrese su telefono" required autocomplete="off">
    </div>

    <div class="form-group">
        <label>Dirección</label>
        <input type="text" name="direccion" value="<?php echo $cliente->direccion; ?>" class="form-control" placeholder="Ingrese su dirección" required autocomplete="off">
    </div>
        
    
    <hr />
    
    <div class="text-right">
        <button class="btn btn-primary">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-alumno").submit(function(){
            return $(this).validate();
        });
    })
</script>