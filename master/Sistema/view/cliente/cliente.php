<h2 class="page-header" >Modulo Clientes</h2>

    <a href="?c=cliente&a=excel" class="btn btn-info">Excel</a>
    <a href="?c=cliente&a=word" class="btn btn-info">Word</a>
    <a class="btn btn-primary pull-right" href="?c=cliente&a=Crud">Nuevo Cliente</a>
<br><br><br>

<table class="table  table-striped  table-hover" id="tabla">
    <thead>
        <tr>
        
            <th style="width:180px; background-color: #5DACCD; color:#fff">Nombre</th>
            <th style=" background-color: #5DACCD; color:#fff">Apellido</th>
            <th style=" background-color: #5DACCD; color:#fff">Teléfono</th>
            <th style="width:120px; background-color: #5DACCD; color:#fff">Dirección</th>            
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            <td><?php echo $r->nombre; ?></td>
            <td><?php echo $r->apellido; ?></td>
            <td><?php echo $r->telefono; ?></td>
            <td><?php echo $r->direccion; ?></td>
            <td>
                <a  class="btn btn-warning" href="?c=cliente&a=Crud&idcliente=<?php echo $r->idcliente; ?>">Editar</a>
            </td>
            <td>
                <a  class="btn btn-danger" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=cliente&a=Eliminar&idcliente=<?php echo $r->idcliente; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 

</body>
<script  src="assets/js/datatable.js">  

</script>


</html>
