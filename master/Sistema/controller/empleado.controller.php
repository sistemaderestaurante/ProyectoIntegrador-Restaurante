<?php
//modelo relacionado con el controller
require_once 'model/empleado.php';

class empleadoController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new empleado();
    }
    //archivos que tiene relacion con este controller
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/empleado/empleado.php';
       
    }
    //Obtener de la base de datos el registro identificandolo con el id
    public function Crud(){
        $empleado = new empleado();
        
        if(isset($_REQUEST['idempleado'])){
            $empleado = $this->model->Obtener($_REQUEST['idempleado']);
        }
        
        require_once 'view/header.php';
        require_once 'view/empleado/empleado-editar.php';
        
    }
    //Actualizar y Guardar un nuevo registro detallando cada campo de la tabla
    public function Guardar(){
        $empleado = new empleado();
        
        $empleado->idempleado = $_REQUEST['idempleado'];
        $empleado->nombre = $_REQUEST['nombre'];
        $empleado->fecha_nacimiento = $_REQUEST['fecha_nacimiento'];
        $empleado->direccion = $_REQUEST['direccion']; 
        $empleado->telefono = $_REQUEST['telefono'];
        $empleado->cargo = $_REQUEST['cargo'];
        $empleado->estado = $_REQUEST['estado'];
        $empleado->usuario = $_REQUEST['usuario']; 

        $empleado->idempleado > 0 
            ? $this->model->Actualizar($empleado)
            : $this->model->Registrar($empleado);
        
        header('Location: indexEmpleado.php');
    }
    //Eliminar un registro identificandolo con el id
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['idempleado']);
        header('Location: indexEmpleado.php');
    }
    public function Excel(){
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=Empleados.xls");
        header("Pragma: no-cache");
        header("Expires: 0");    
        
        require_once 'view/empleado/empleado-excel.php';
    }

    public function Word(){
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment; filename=Empleados.doc");
        header("Pragma: no-cache");
        header("Expires: 0");    
        
        require_once 'view/empleado/empleado-word.php';
    }

}