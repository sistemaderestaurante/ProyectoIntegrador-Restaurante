<?php
//modelo relacionado con el controller
require_once 'model/reservacion.php';

class reservacionController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new reservacion();
    }
    //archivos que tiene relacion con este controller
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/reservacion/reservacion.php';
       
    }
    //Obtener de la base de datos el registro identificandolo con el id
    public function Crud(){
        $reservacion = new reservacion();
        
        if(isset($_REQUEST['idreservacion'])){
            $reservacion = $this->model->Obtener($_REQUEST['idreservacion']);
        }
        
        require_once 'view/header.php';
        require_once 'view/reservacion/reservacion-editar.php';
        
    }
    //Actualizar y Guardar un nuevo registro detallando cada campo de la tabla
    public function Guardar(){
        $reservacion = new reservacion();
        
        $reservacion->idreservacion = $_REQUEST['idreservacion'];
        $reservacion->nombrecliente = $_REQUEST['nombrecliente'];
        $reservacion->personas = $_REQUEST['personas'];
        $reservacion->telefono = $_REQUEST['telefono']; 
        $reservacion->dia = $_REQUEST['dia'];
        $reservacion->hora = $_REQUEST['hora'];
        $reservacion->observaciones = $_REQUEST['observaciones'];
        $reservacion->mesa = $_REQUEST['mesa'];

            
      

        $reservacion->idreservacion > 0 
            ? $this->model->Actualizar($reservacion)
            : $this->model->Registrar($reservacion);
        
        header('Location: indexReservacion.php');
    }
    //Eliminar un registro identificandolo con el id
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['idreservacion']);
        header('Location: indexReservacion.php');
    }

    public function Excel(){
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=Reservaciones.xls");
        header("Pragma: no-cache");
        header("Expires: 0");    
        
        require_once 'view/reservacion/reservacion-excel.php';
    }

    public function Word(){
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment; filename=Reservaciones.doc");
        header("Pragma: no-cache");
        header("Expires: 0");    
        
        require_once 'view/reservacion/reservacion-word.php';
    }
}