<?php
//modelo relacionado con el controller
require_once 'model/cliente.php';

class clienteController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new cliente();
    }
    //archivos que tiene relacion con este controller
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/cliente/cliente.php';
       
    }
    //Obtener de la base de datos el registro identificandolo con el id
    public function Crud(){
        $cliente = new cliente();
        
        if(isset($_REQUEST['idcliente'])){
            $cliente = $this->model->Obtener($_REQUEST['idcliente']);
        }
        
        require_once 'view/header.php';
        require_once 'view/cliente/cliente-editar.php';
        
    }
    //Actualizar y Guardar un nuevo registro detallando cada campo de la tabla
    public function Guardar(){
        $cliente = new cliente();
        
        $cliente->idcliente = $_REQUEST['idcliente'];
        $cliente->nombre = $_REQUEST['nombre'];
        $cliente->apellido = $_REQUEST['apellido'];
        $cliente->telefono = $_REQUEST['telefono']; 
        $cliente->direccion = $_REQUEST['direccion']; 
            
      

        $cliente->idcliente > 0 
            ? $this->model->Actualizar($cliente)
            : $this->model->Registrar($cliente);
        
        header('Location: indexCliente.php');
    }
    //Eliminar un registro identificandolo con el id
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['idcliente']);
        header('Location: indexCliente.php');
    }

    public function Excel(){
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=Clientes.xls");
        header("Pragma: no-cache");
        header("Expires: 0");    
        
        require_once 'view/cliente/cliente-excel.php';
    }

    public function Word(){
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment; filename=Clientes.doc");
        header("Pragma: no-cache");
        header("Expires: 0");    
        
        require_once 'view/cliente/cliente-word.php';
    }

}