<?php
//modelo relacionado con el controller
require_once 'model/categoria.php';
//clase
class categoriaController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new categoria();
    }
    //archivos que tiene relacion con este controller
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/categoria/categoria.php';
       
    }
    //Obtener de la base de datos el registro identificandolo con el id
    public function Crud(){
        $categoria = new categoria();
        
        if(isset($_REQUEST['idcategoria'])){
            $categoria = $this->model->Obtener($_REQUEST['idcategoria']);
        }
        
        require_once 'view/header.php';
        require_once 'view/categoria/categoria-editar.php';
        
    }
    //Actualizar y Guardar un nuevo registro detallando cada campo de la tabla
    public function Guardar(){
        $categoria = new categoria();
        
        $categoria->idcategoria = $_REQUEST['idcategoria'];
        $categoria->nombrecategoria = $_REQUEST['nombrecategoria'];
         
        $categoria->idcategoria > 0 
            ? $this->model->Actualizar($categoria)
            : $this->model->Registrar($categoria);
        
        header('Location: indexCategoria.php');
    }
    //Eliminar un registro identificandolo con el id
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['idcategoria']);
        header('Location: indexCategoria.php');
    }

}