<?php
//modelo relacionado con el controller
require_once 'model/usuario.php';

class usuarioController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new usuario();
    }
    //archivos que tiene relacion con este controller
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/usuario/usuario.php';
       
    }
    //Obtener de la base de datos el registro identificandolo con el id
    public function Crud(){
        $usuario = new usuario();
        
        if(isset($_REQUEST['idusuario'])){
            $usuario = $this->model->Obtener($_REQUEST['idusuario']);
        }
        
        require_once 'view/header.php';
        require_once 'view/usuario/usuario-editar.php';
        
    }
    //Actualizar y Guardar un nuevo registro detallando cada campo de la tabla
    public function Guardar(){
        $usuario = new usuario();
        
        $usuario->idusuario = $_REQUEST['idusuario'];
        $usuario->usuario = $_REQUEST['usuario'];
        $usuario->pass = $_REQUEST['pass']; 
            
      

        $usuario->idusuario > 0 
            ? $this->model->Actualizar($usuario)
            : $this->model->Registrar($usuario);
        
        header('Location: indexUsuario.php');
    }
    //Eliminar un registro identificandolo con el id
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['idusuario']);
        header('Location: indexUsuario.php');
    }

}