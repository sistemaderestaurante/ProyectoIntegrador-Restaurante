<?php
//modelo relacionado con el controller
require_once 'model/producto.php';

class productoController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new producto();
    }
    //archivos que tiene relacion con este controller
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/producto/producto.php';
       
    }
    //Obtener de la base de datos el registro identificandolo con el id
    public function Crud(){
        $producto = new producto();
        
        if(isset($_REQUEST['idproducto'])){
            $producto = $this->model->Obtener($_REQUEST['idproducto']);
        }
        
        require_once 'view/header.php';
        require_once 'view/producto/producto-editar.php';
        
    }
    //Actualizar y Guardar un nuevo registro detallando cada campo de la tabla
    public function Guardar(){
        $producto = new producto();
        
        $producto->idproducto = $_REQUEST['idproducto'];
        $producto->nombre = $_REQUEST['nombre'];
        $producto->precio = $_REQUEST['precio'];
        $producto->categoria = $_REQUEST['categoria'];

        $producto->idproducto > 0 
            ? $this->model->Actualizar($producto)
            : $this->model->Registrar($producto);
        
        header('Location: indexProducto.php');
    }
    //Eliminar un registro identificandolo con el id
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['idproducto']);
        header('Location: indexProducto.php');
    }

}