<?php
class usuario
{
	private $pdo;
    //variables
    public $idusuario;
    public $usuario;
    public $pass;
//funcion de conexion del modelo con la base de datos
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
//funcion que realiza una consulta a la tabla usuario para listar los registros
	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM usuarios");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
//obtiene los registros realizando una consulta con la clausula where que especifica que se buscara con el id del registro
	public function Obtener($idusuario)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM usuarios WHERE idusuario = ?");
			          

			$stm->execute(array($idusuario));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
//elimina los registros realizando una consulta con la clausula where que especifica que se buscara y se eliminara por del id del registro
	public function Eliminar($idusuario)
	{
		try 
		{
			$stm = $this->pdo
			            ->prepare("DELETE FROM usuarios WHERE idusuario = ?");			          

			$stm->execute(array($idusuario));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
//actualiza los registros realizando una consulta con la clausula where que especifica que se buscara y se actualizara el registro por medio del id
	public function Actualizar($data)
	{
		try 
		{
			$sql = "UPDATE usuarios SET 
						usuario         = ?, 
						pass            = ?
						
				    WHERE idusuario = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array( 
                        $data->usuario,                        
                        $data->pass, 
                        $data->idusuario
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
  //guarda un nuevo registro realizando un insert en la base de datos
	public function Registrar(usuario $data)
	{
		try 
		{
		$sql = "INSERT INTO usuarios (usuario,pass) 
		        VALUES (?, ?)";
//columnas con las que cuenta la tabla
		$this->pdo->prepare($sql)
		     ->execute(
				array( 
                    $data->usuario,
                    $data->pass 
                   
                )
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}