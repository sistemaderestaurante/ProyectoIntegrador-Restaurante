<?php
class producto
{
	private $pdo;
    //variables
    public $idproducto;
    public $nombre;  
    public $precio;
    public $categoria; 
//funcion de conexion del modelo con la base de datos
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
//funcion que realiza una consulta a la tabla producto para listar los registros
	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM producto");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
//obtiene los registros realizando una consulta con la clausula where que especifica que se buscara con el id del registro
	public function Obtener($idproducto)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM producto WHERE idproducto = ?");
			          

			$stm->execute(array($idproducto));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
//elimina los registros realizando una consulta con la clausula where que especifica que se buscara y se eliminara por del id del registro
	public function Eliminar($idproducto)
	{
		try 
		{
			$stm = $this->pdo
			            ->prepare("DELETE FROM producto WHERE idproducto = ?");			          

			$stm->execute(array($idproducto));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
//actualiza los registros realizando una consulta con la clausula where que especifica que se buscara y se actualizara el registro por medio del id
	public function Actualizar($data)
	{
		try 
		{
			$sql = "UPDATE producto SET  
						nombre               = ?,
						precio               = ?,
						categoria            = ?
						
				    WHERE idproducto = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(                       
                        $data->nombre,
                        $data->precio,
                        $data->categoria,  
                        $data->idproducto
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
  //guarda un nuevo registro realizando un insert en la base de datos
	public function Registrar(producto $data)
	{
		try 
		{
		$sql = "INSERT INTO producto (nombre,precio,categoria) 
		        VALUES (?, ?, ?)";
//columnas con las que cuenta la tabla
		$this->pdo->prepare($sql)
		     ->execute(
				array( 
                    $data->nombre,   
                    $data->precio,
                    $data->categoria
                )
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
    }
}