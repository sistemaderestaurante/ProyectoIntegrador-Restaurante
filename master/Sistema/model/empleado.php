<?php
class empleado
{
	private $pdo;
    //variables
    public $idempleado;
    public $nombre;
    public $fecha_nacimiento;  
    public $direccion;
    public $telefono;
    public $cargo;
    public $estado;
    public $usuario;
//funcion de conexion del modelo con la base de datos
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
 //funcion que realiza una consulta a la tabla empleados para listar los registros
	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM empleados");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
//obtiene los registros realizando una consulta con la clausula where que especifica que se buscara con el id del registro
	public function Obtener($idempleado)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM empleados WHERE idempleado = ?");
			          

			$stm->execute(array($idempleado));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
//elimina los registros realizando una consulta con la clausula where que especifica que se buscara y se eliminara por del id del registro
	public function Eliminar($idempleado)
	{
		try 
		{
			$stm = $this->pdo
			            ->prepare("DELETE FROM empleados WHERE idempleado = ?");			          

			$stm->execute(array($idempleado));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
 //actualiza los registros realizando una consulta con la clausula where que especifica que se buscara y se actualizara el registro por medio del id
	public function Actualizar($data)
	{
		try 
		{
			$sql = "UPDATE empleados SET 
						nombre                  = ?, 
						fecha_nacimiento        = ?,
						direccion               = ?,
                        telefono                = ?,
                        cargo                   = ?,
                        estado                  = ?,
                        usuario                 = ?
						
				    WHERE idempleado = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array( 
                        $data->nombre,                    
                        $data->fecha_nacimiento,
                        $data->direccion,
                        $data->telefono, 
                        $data->cargo,
                        $data->estado,
                        $data->usuario,
                        $data->idempleado
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
   //guarda un nuevo registro realizando un insert en la base de datos
	public function Registrar(empleado $data)
	{
		try 
		{
		$sql = "INSERT INTO empleados (nombre,fecha_nacimiento,direccion,telefono,cargo,estado,usuario) 
		        VALUES (?, ?, ?, ?, ?, ?, ?)";
//columnas con las que cuenta la tabla
		$this->pdo->prepare($sql)
		     ->execute(
				array( 
                    $data->nombre,
                    $data->fecha_nacimiento,
                    $data->direccion,
                    $data->telefono, 
                    $data->cargo,
                    $data->estado,
                    $data->usuario
                   
                )
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}