<?php
class categoria
{
	private $pdo;
    

   //variables 
    public $idcategoria;
    public $nombrecategoria;
    //funcion de conexion del modelo con la base de datos
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
    
    //funcion que realiza una consulta a la tabla categoria para listar los registros
	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM categoria");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
    
    //obtiene los registros realizando una consulta con la clausula where que especifica que se buscara con el id del registro
	public function Obtener($idcategoria)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM categoria WHERE idcategoria = ?");
			          

			$stm->execute(array($idcategoria));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

     //elimina los registros realizando una consulta con la clausula where que especifica que se buscara y se eliminara por del id del registro
	public function Eliminar($idcategoria)
	{
		try 
		{
			$stm = $this->pdo
			            ->prepare("DELETE FROM categoria WHERE idcategoria = ?");			          

			$stm->execute(array($idcategoria));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

    //actualiza los registros realizando una consulta con la clausula where que especifica que se buscara y se actualizara el registro por medio del id
	public function Actualizar($data)
	{
		try 
		{
			$sql = "UPDATE categoria SET 
						nombrecategoria          = ? 
					
				        WHERE idcategoria = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array( 
                        $data->nombrecategoria,                        
                        $data->idcategoria
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
    //guarda un nuevo registro realizando un insert en la base de datos
	public function Registrar(categoria $data)
	{
		try 
		{
		$sql = "INSERT INTO categoria (nombrecategoria) 
		        VALUES (?)";
        //columnas con las qu cuenta la tabla
		$this->pdo->prepare($sql)
		     ->execute(
				array( 
                    $data->nombrecategoria
                   
                )
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}