<?php
class cliente
{
	private $pdo;
    
    //variables
    public $idcliente;
    public $nombre;
    public $apellido;  
    public $telefono;
    public $direccion;
//funcion de conexion del modelo con la base de datos
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
 //funcion que realiza una consulta a la tabla cliente para listar los registros
	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM cliente");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
//obtiene los registros realizando una consulta con la clausula where que especifica que se buscara con el id del registro
	public function Obtener($idcliente)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM cliente WHERE idcliente = ?");
			          

			$stm->execute(array($idcliente));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
 //elimina los registros realizando una consulta con la clausula where que especifica que se buscara y se eliminara por del id del registro
	public function Eliminar($idcliente)
	{
		try 
		{
			$stm = $this->pdo
			            ->prepare("DELETE FROM cliente WHERE idcliente = ?");			          

			$stm->execute(array($idcliente));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
 //actualiza los registros realizando una consulta con la clausula where que especifica que se buscara y se actualizara el registro por medio del id
	public function Actualizar($data)
	{
		try 
		{
			$sql = "UPDATE cliente SET 
						nombre          = ?, 
						apellido        = ?,
						telefono        = ?,
                        direccion        = ?
						
				    WHERE idcliente = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array( 
                        $data->nombre,                        
                        $data->apellido,
                         $data->telefono,
                        $data->direccion, 
                        $data->idcliente
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
    //guarda un nuevo registro realizando un insert en la base de datos
	public function Registrar(cliente $data)
	{
		try 
		{
		$sql = "INSERT INTO cliente (nombre,apellido,telefono,direccion) 
		        VALUES (?, ?, ?, ?)";
//columnas con las que cuenta la tabla
		$this->pdo->prepare($sql)
		     ->execute(
				array( 
                    $data->nombre,
                    $data->apellido, 
                    $data->telefono, 
                    $data->direccion 
                   
                )
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}