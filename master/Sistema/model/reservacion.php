<?php
class reservacion
{
	private $pdo;
    //variables
    public $idreservacion;
    public $nombrecliente;
    public $personas;  
    public $telefono;
    public $dia;
    public $hora;
    public $observaciones;
    public $mesa;
//funcion de conexion del modelo con la base de datos
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
//funcion que realiza una consulta a la tabla reservacion para listar los registros
	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM reservas");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
//obtiene los registros realizando una consulta con la clausula where que especifica que se buscara con el id del registro
	public function Obtener($ididreservacion)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM reservas WHERE idreservacion = ?");
			          

			$stm->execute(array($ididreservacion));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
//elimina los registros realizando una consulta con la clausula where que especifica que se buscara y se eliminara por del id del registro
	public function Eliminar($idreservacion)
	{
		try 
		{
			$stm = $this->pdo
			            ->prepare("DELETE FROM reservas WHERE idreservacion = ?");			          

			$stm->execute(array($idreservacion));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
//actualiza los registros realizando una consulta con la clausula where que especifica que se buscara y se actualizara el registro por medio del id
	public function Actualizar($data)
	{
		try 
		{
			$sql = "UPDATE reservas SET 
						nombrecliente        = ?, 
						personas             = ?,
						telefono             = ?,
                        dia                  = ?,
                        hora                 = ?,
                        observaciones        = ?,
                        mesa                 = ?
						
				                    WHERE idreservacion = ?";


			$this->pdo->prepare($sql)
			     ->execute(
				    array( 
                        $data->nombrecliente,                        
                        $data->personas,
                        $data->telefono,
                        $data->dia, 
                        $data->hora, 
                        $data->observaciones,
                        $data->mesa,  
                        $data->idreservacion
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
  //guarda un nuevo registro realizando un insert en la base de datos
	public function Registrar(reservacion $data)
	{
		try 
		{
		$sql = "INSERT INTO reservas (nombrecliente,personas,telefono,dia,hora,observaciones,mesa) 
		        VALUES (?, ?, ?, ?, ?, ?, ?)";

//columnas con las que cuenta la tabla
		$this->pdo->prepare($sql)
		     ->execute(
				array( 
                    $data->nombrecliente,
                    $data->personas, 
                    $data->telefono, 
                    $data->dia,
                    $data->hora,
                    $data->observaciones,
                    $data->mesa

                   
                )
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}